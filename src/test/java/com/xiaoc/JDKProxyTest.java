package com.xiaoc;/**
 * Created by fanxiaochun on 2018/6/24.
 */

import com.xiaoc.handler.UserInvocationHandler;
import com.xiaoc.proxy.JDKProxyFactory;
import com.xiaoc.service.UserService;
import com.xiaoc.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author
 * @create 2018/6/24
 */
public class JDKProxyTest {

    @Test
    public void userServiceTest() {
        UserService userService = JDKProxyFactory.getProxy(UserServiceImpl.class, new UserInvocationHandler(new UserServiceImpl()));
        Assert.assertEquals("hello world", userService.getName());
    }
}
