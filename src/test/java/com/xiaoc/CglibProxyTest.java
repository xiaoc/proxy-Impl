package com.xiaoc;/**
 * Created by fanxiaochun on 2018/6/24.
 */

import com.xiaoc.handler.UserServiceInterceptor;
import com.xiaoc.proxy.CglibProxyFactory;
import com.xiaoc.service.UserService;
import com.xiaoc.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author fanxiaochun
 * @create 2018/6/24
 */
public class CglibProxyTest {

    @Test
    public void userServiceTest() {
        UserService userService = CglibProxyFactory.getProxy(UserServiceImpl.class, new UserServiceInterceptor());
        Assert.assertEquals("hello world", userService.getName());
    }
}
