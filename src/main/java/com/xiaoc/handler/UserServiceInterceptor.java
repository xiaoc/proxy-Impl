package com.xiaoc.handler;/**
 * Created by fanxiaochun on 2018/6/24.
 */

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author fanxiaochun
 * @create 2018/6/24
 */
public class UserServiceInterceptor implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("调用前....");
        Object result = methodProxy.invokeSuper(o, objects);
        System.out.println("调用后....");
        return result;
    }
}
