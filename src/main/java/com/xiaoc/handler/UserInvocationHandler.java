package com.xiaoc.handler;/**
 * Created by fanxiaochun on 2018/6/24.
 */

import com.xiaoc.service.UserService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author
 * @create 2018/6/24
 */
public class UserInvocationHandler implements InvocationHandler {

    private UserService userService;

    public UserInvocationHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("调用前....");
        Object result = method.invoke(this.userService, args);
        System.out.println("调用后....");
        return result;
    }
}
