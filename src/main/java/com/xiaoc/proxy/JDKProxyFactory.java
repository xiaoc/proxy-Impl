package com.xiaoc.proxy;/**
 * Created by fanxiaochun on 2018/6/24.
 */

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * JDK 动态代理
 *
 * @author
 * @create 2018/6/24
 */
public class JDKProxyFactory {

    /**
     * @param tClass            目标代理对象类
     * @param invocationHandler 代理拦截处理类
     * @param <T>
     * @return
     */
    public static <T> T getProxy(Class<T> tClass, InvocationHandler invocationHandler) {
        return (T) Proxy.newProxyInstance(tClass.getClassLoader(), tClass.getInterfaces(), invocationHandler);
    }
}
