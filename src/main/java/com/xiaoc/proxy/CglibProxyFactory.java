package com.xiaoc.proxy;/**
 * Created by fanxiaochun on 2018/6/24.
 */

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

/**
 * Cglib  动态代理
 *
 * @author
 * @create 2018/6/24
 */
public class CglibProxyFactory {

    /**
     * @param tClass            目标代理对象类
     * @param methodInterceptor 方法拦截对象
     * @param <T>
     * @return
     */
    public static  <T> T getProxy(Class<T> tClass, MethodInterceptor methodInterceptor) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(tClass);
        enhancer.setCallback(methodInterceptor);
        return (T) enhancer.create();
    }
}
